import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppPreloading } from './app.preloading';
import * as fromGuards from './core/authentication/guards';

import {
  AuthenticationLoginComponent,
  AuthenticationLogoutComponent
} from './core/authentication/containers';

import { HomeComponent, ErrorComponent } from './pages';
import { MainComponent } from './core/layouts/one-column/main/main.component';
import {
  CONTACTS_RESOLVER,
  LANGUAGES_RESOLVER
} from './core/layouts/components/';

const appRoutes: Routes = [
  { path: '', redirectTo: 'main/dashboard', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, data: { state: 'home' } },
  { path: 'login', component: AuthenticationLoginComponent, data: { state: 'login' } },
  { path: 'logout', component: AuthenticationLogoutComponent, canActivate: [...fromGuards.guards], data: { state: 'logout' } },
  {
    path: 'main', component: MainComponent, canActivate: [...fromGuards.guards], resolve: {
      contacts: CONTACTS_RESOLVER,
      languages: LANGUAGES_RESOLVER
    },
    children: [
      { path: 'dashboard', loadChildren: './modules/dashboard/dashboard.module#DashboardModule', data: { state: 'dashboard' } },
      { path: 'apartments', loadChildren: './modules/apartments/apartments.module#ApartmentsModule', data: { state: 'apartments' } },
    ]
  },
  { path: 'not-found', component: ErrorComponent, data: { message: 'Page not found!' } },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes/*,{ preloadingStrategy: AppPreloading }*/)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
