import { InjectionToken } from '@angular/core';
import { AppApi } from './app.api.model';

export const API_URLS_TOKEN = new InjectionToken<AppApi>('app.api');
