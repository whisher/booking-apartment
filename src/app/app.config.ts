import { environment } from '../environments/environment';
import { AppApi } from './app.api.model';

export function urls(): AppApi {
  return {
    apartments: {
      search: `${environment.BASE_URL}api/backend/apartament/querypagination`,
      load: `${environment.BASE_URL}api/backend/apartament/querypagination/filter`,
      refresh: (lang, querykey, pagesize) => {
        return `${environment.BASE_URL}api/backend/apartament/querypagination/refresh/${lang}/${querykey}/${pagesize}`;
      },
      getById: (lang, id, querykey) => {
        return `${environment.BASE_URL}api/backend/apartment/${lang}/${id}/${querykey}`;
      }
    },
    authentication: {
      login: `${environment.BASE_URL}oauth/tokens`,
      logout: `${environment.BASE_URL}api/logout`,
      account: `${environment.BASE_URL}api/account`
    },
    contact: lang => {
      return `${environment.BASE_URL}api/backend/contacts/${lang}`;
    },
    language: lang => {
      return `${environment.BASE_URL}api/backend/language/${lang}`;
    }
  };
}

export const API_URLS: AppApi = urls();
