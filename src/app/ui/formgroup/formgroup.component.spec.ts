import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UiFormgroupComponent } from './formgroup.component';

describe('FormgroupComponent', () => {
  let component: UiFormgroupComponent;
  let fixture: ComponentFixture<UiFormgroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UiFormgroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UiFormgroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
