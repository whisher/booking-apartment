import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[uiFormgroup]'
})
export class UiFormgroupDirective {
  focus = false;

    @HostListener('focus')
    onFocus() {
      this.focus = true;
    }

    @HostListener('blur')
    onBlur() {
      this.focus = false;
  }

}
