import { Component, HostBinding, ContentChild } from '@angular/core';

import { UiFormgroupDirective } from './formgroup.directive';

@Component({
  selector: 'ui-form-group',
  templateUrl: './formgroup.component.html',
  styleUrls: ['./formgroup.component.scss']
})
export class UiFormgroupComponent {
  @ContentChild(UiFormgroupDirective)
  input: UiFormgroupDirective;
  @HostBinding('class.hasFocus')
  get focus() {
    return this.input ? this.input.focus : false;
  }
  constructor() { }
}
