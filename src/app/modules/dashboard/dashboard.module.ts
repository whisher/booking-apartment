import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';

import {
  DashboardComponent
} from './';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    DashboardRoutingModule
  ],
  exports: [
    DashboardComponent
  ],
  declarations: [
    DashboardComponent
  ]
})
export class DashboardModule { }
