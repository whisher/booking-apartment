import { InjectionToken } from '@angular/core';

export const APARTMENTS_ENTITY_SERVICE = new InjectionToken('apartments.entity.service');
