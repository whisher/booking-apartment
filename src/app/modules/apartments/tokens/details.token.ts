import { InjectionToken } from '@angular/core';

export const APARTMENTS_DETAILS_SERVICE = new InjectionToken('apartments.details.service');
