export interface AttributeValue { }

export interface OrderInfo {
  name: string;
  price: string;
}

export interface ApartmentsSearchParams {
  attributeValues: AttributeValue[];
  filterValues: AttributeValue[];
  idProductcategory: number;
  language: string;
  orderInfo: OrderInfo;
  page: number;
  pageSize: number;
}
