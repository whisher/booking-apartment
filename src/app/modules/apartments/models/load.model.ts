
export interface Id {
}

export interface Area {
  id: Id;
  name: string;
  type: string;
}

export interface Id2 {
}

export interface Pois {
  id: Id2;
  name: string;
  type: string;
}

export interface Filter {
  areas: Area[];
  bookingOnLine: boolean;
  corporate: boolean;
  nameOrId: string;
  pois: Pois[];
}

export interface OrderInfo {
  name: string;
  price: string;
}

export interface ApartmentsLoadParams {
  filter: Filter;
  language: string;
  orderInfo: OrderInfo;
  page: number;
  pageSize: number;
  queryKey: string;
}
