export interface Calendar {
  enable: boolean;
  lastUpdate: string;
  realTime: boolean;
}

export interface FirstPrice { }

export interface Area {
  id: number;
  name: string;
}

export interface Geopos {
  addressGoogle: string;
  lat: number;
  lon: number;
  zoom: number;
}

export interface Pois {
  id: number;
  name: string;
}

export interface Location {
  address: string;
  areas: Area[];
  city: string;
  country: string;
  geopos: Geopos;
  pois: Pois[];
  zipcode: string;
}

export interface SecondPrice { }

export interface Apartment {
  apartmentCorporate: boolean;
  attribute1: number;
  attribute2: number;
  attribute3: number;
  calendar: Calendar;
  canUpdate: boolean;
  enableBookNowBtn: boolean;
  enableOnRequestBtn: boolean;
  firstPrice: FirstPrice;
  id: number;
  imageBig: string;
  imageSmall: string;
  location: Location;
  secondPrice: SecondPrice;
  showFirstPrice: boolean;
  showSecondPrice: boolean;
  title: string;
  querykey: string;
}

export interface City {
  id: number;
  lat: number;
  lng: number;
  name: string;
}

export interface Id { }

export interface Area2 {
  id: Id;
  name: string;
  type: string;
}

export interface Id2 { }

export interface Pois2 {
  id: Id2;
  name: string;
  type: string;
}

export interface Filter {
  areas: Area2[];
  bookingOnLine: boolean;
  corporate: boolean;
  nameOrId: string;
  pois: Pois2[];
}

export interface OrderInfo {
  name: string;
  price: string;
}

export interface PaginationInfo {
  city: City;
  currentPage: number;
  end: number;
  filter: Filter;
  hasNext: boolean;
  hasPrevious: boolean;
  lang: string;
  numTotItems: number;
  numTotPages: number;
  orderInfo: OrderInfo;
  pageSize: number;
  querykey: string;
  start: number;
}

export interface SwitchPrice {
  doublePrice: boolean;
  firstLabel: string;
  secondLabel: string;
}

export interface Tag {
  label: string;
  value: string;
}

export interface ApartmentsEntity {
  data: Apartment[];
  paginationInfo: PaginationInfo;
  switchPrice: SwitchPrice;
  tags: Tag[];
}
