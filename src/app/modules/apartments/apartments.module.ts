import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '@app/shared/shared.module';
import { ApartmentsRoutingModule } from './apartments-routing.module';


import * as fromComponents from './components';
import * as fromContainers from './containers';
import * as fromGuards from './guards';
import * as fromServices from './services';

import { reducers, effects } from './store';

@NgModule({
  imports: [
    RouterModule,
    ApartmentsRoutingModule,
    SharedModule,
    StoreModule.forFeature('properties', reducers),
    EffectsModule.forFeature(effects)
  ],
  declarations: [...fromContainers.containers, ...fromComponents.components],
  exports: [...fromContainers.containers, ...fromComponents.components],
  providers: [...fromServices.services, ...fromGuards.guards]
})
export class ApartmentsModule { }
