import { Component, OnInit, Input } from '@angular/core';

import { Apartment, PaginationInfo } from '../../models/entity.model';

@Component({
  selector: 'tnos-apartments-row',
  templateUrl: './apartments-row.component.html',
  styleUrls: ['./apartments-row.component.scss']
})
export class ApartmentsRowComponent implements OnInit {
  @Input() items: Apartment[];
  @Input() querykey: string;
  @Input() pageSize: number;
  constructor() { }

  ngOnInit() {
  }

}
