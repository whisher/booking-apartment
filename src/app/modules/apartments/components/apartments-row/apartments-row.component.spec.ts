import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentsRowComponent } from './apartments-row.component';

describe('ApartmentsRowComponent', () => {
  let component: ApartmentsRowComponent;
  let fixture: ComponentFixture<ApartmentsRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApartmentsRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentsRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
