import { Component, ChangeDetectionStrategy, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';

import { debounceTime } from 'rxjs/operators';


import {
  PaginationInfo
} from '../../models/entity.model';

@Component({
  selector: 'tnos-apartments-filters',
  templateUrl: './apartments-filters.component.html',
  styleUrls: ['./apartments-filters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ApartmentsFiltersComponent implements OnInit {
  @Input() info: PaginationInfo = {} as PaginationInfo;
  @Output() update = new EventEmitter<any>();
  @Output() paginator = new EventEmitter<any>();
  filtersForm: FormGroup;
  paginationRanges = [10, 20, 30];

  constructor() {
    this.filtersForm = new FormGroup({
      bookingOnLine: new FormControl(false),
      corporate: new FormControl(false),
      nameOrId: new FormControl(''),
      pageSize: new FormControl()
    });
  }
  ngOnInit() {
    this.filtersForm.valueChanges
      .pipe(
      debounceTime(500)
      )
      .subscribe(
      (values) => {
        this.onFilterUpdate(values);
      }
      );
  }
  onFilterUpdate(values) {
    const info = {
      filter: {
        bookingOnLine: values.bookingOnLine,
        corporate: values.corporate,
        nameOrId: values.nameOrId,
        pois: [],
        areas: []
      },
      pageSize: values.pageSize,
      language: this.info.lang,
      orderInfo: this.info.orderInfo,
      page: this.info.currentPage,
      queryKey: this.info.querykey
    };
    this.update.emit(info);
  }
  previous() {
    this.paginator.emit(this.info.currentPage);
  }
  next() {
    this.paginator.emit(this.info.currentPage);
  }
}
