import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentsFiltersComponent } from './apartments-filters.component';

describe('ApartmentsFilterComponent', () => {
  let component: ApartmentsFiltersComponent;
  let fixture: ComponentFixture<ApartmentsFiltersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ApartmentsFiltersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentsFiltersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
