import { ApartmentsFiltersComponent } from './apartments-filters/apartments-filters.component';
import { ApartmentsRowComponent } from './apartments-row/apartments-row.component';

export const components: any[] = [
  ApartmentsFiltersComponent,
  ApartmentsRowComponent
];

export * from './apartments-filters/apartments-filters.component';
export * from './apartments-row/apartments-row.component';
