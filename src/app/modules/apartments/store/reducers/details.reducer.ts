import * as fromApartmentDetailsActions from '../actions/details.actions';

import {
  Apartment
} from '../../models/entity.model';

export interface ApartmentDetailsState {
  data: Apartment;
  loaded: boolean;
  loading: boolean;
}

const initialState: ApartmentDetailsState = {
  data: {} as Apartment,
  loaded: false,
  loading: false
};

export function apartmentDetailsReducer(
  state = initialState,
  action: fromApartmentDetailsActions.ApartmentDetailsActions
): ApartmentDetailsState {
  switch (action.type) {
    case (fromApartmentDetailsActions.APARTMENT_DETAILS):
      return {
        ...state,
        loading: true
      };
    case (fromApartmentDetailsActions.APARTMENT_DETAILS_SUCCESS):
      return {
        ...state,
        loading: false,
        loaded: true,
        data: { ...action.payload }
      };
    case (fromApartmentDetailsActions.APARTMENT_DETAILS_FAIL):
      return {
        ...state,
        loading: false,
        loaded: false
      };
    default:
      return state;
  }
}

export const getApartmentDetails = (state: ApartmentDetailsState) => state.data;
export const getApartmentDetailsLoaded = (state: ApartmentDetailsState) => state.loaded;
export const getApartmentDetailsLoading = (state: ApartmentDetailsState) => state.loading;
