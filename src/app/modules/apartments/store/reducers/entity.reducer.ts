import * as fromApartmentsEntityActions from '../actions/entity.actions';

import {
  ApartmentsEntity
} from '../../models/entity.model';

export interface ApartmentsEntityState {
  entity: ApartmentsEntity;
  loaded: boolean;
  loading: boolean;
}

const initialState: ApartmentsEntityState = {
  entity: {
    data: [],
    paginationInfo: null,
    switchPrice: null,
    tags: []
  },
  loaded: false,
  loading: false
};

export function apartmentsEntityReducer(
  state = initialState,
  action: fromApartmentsEntityActions.ApartmentsEntityActions
): ApartmentsEntityState {
  switch (action.type) {
    case (fromApartmentsEntityActions.APARTMENTS_SEARCH):
      return {
        ...state,
        loading: true
      };
    case (fromApartmentsEntityActions.APARTMENTS_SEARCH_SUCCESS):
      return {
        ...state,
        loading: false,
        loaded: true,
        entity: { ...action.payload }
      };
    case (fromApartmentsEntityActions.APARTMENTS_SEARCH_FAIL):
      return {
        ...state,
        loading: false,
        loaded: false
      };
    case (fromApartmentsEntityActions.APARTMENTS_LOAD):
      return {
        ...state,
        loading: true
      };
    case (fromApartmentsEntityActions.APARTMENTS_LOAD_SUCCESS):
      return {
        ...state,
        loading: false,
        loaded: true,
        entity: { ...action.payload }
      };
    case (fromApartmentsEntityActions.APARTMENTS_LOAD_FAIL):
      return {
        ...state,
        loading: false,
        loaded: false
      };
    case (fromApartmentsEntityActions.APARTMENTS_REFRESH):
      return {
        ...state,
        loading: true
      };
    case (fromApartmentsEntityActions.APARTMENTS_REFRESH_SUCCESS):
      return {
        ...state,
        loading: false,
        loaded: true,
        entity: { ...action.payload }
      };
    case (fromApartmentsEntityActions.APARTMENTS_REFRESH_FAIL):
      return {
        ...state,
        loading: false,
        loaded: false
      };
    default:
      return state;
  }
}

export const getApartments = (state: ApartmentsEntityState) => state.entity.data;
export const getPaginationInfo = (state: ApartmentsEntityState) => state.entity.paginationInfo;
export const getQuerykey = (state: ApartmentsEntityState) => state.entity.paginationInfo.querykey;
export const getSwitchPrice = (state: ApartmentsEntityState) => state.entity.switchPrice;
export const getTags = (state: ApartmentsEntityState) => state.entity.tags;
export const getApartmentsLoaded = (state: ApartmentsEntityState) => state.loaded;
export const getApartmentsLoading = (state: ApartmentsEntityState) => state.loading;
