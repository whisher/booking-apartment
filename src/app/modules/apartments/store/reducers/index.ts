import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromApartmentsEntity from './entity.reducer';
import * as fromApartmentDetails from './details.reducer';

export interface PropertiesState {
  apartments: fromApartmentsEntity.ApartmentsEntityState;
  apartment: fromApartmentDetails.ApartmentDetailsState;
}

export const reducers: ActionReducerMap<PropertiesState> = {
  apartments: fromApartmentsEntity.apartmentsEntityReducer,
  apartment: fromApartmentDetails.apartmentDetailsReducer
};

export const getPropertiesState = createFeatureSelector<PropertiesState>(
  'properties'
);
