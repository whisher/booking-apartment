import { createSelector } from '@ngrx/store';

import * as fromRoot from '@app/store/reducers';
import * as fromFeature from '../reducers';
import * as fromApartments from '../reducers/entity.reducer';


export const getApartmentsState = createSelector(
  fromFeature.getPropertiesState,
  (state: fromFeature.PropertiesState) => state.apartments
);

export const getApartments = createSelector(getApartmentsState, fromApartments.getApartments);
export const getPaginationInfo = createSelector(getApartmentsState, fromApartments.getPaginationInfo);
export const getQuerykey = createSelector(getApartmentsState, fromApartments.getQuerykey);
export const getSwitchPrice = createSelector(getApartmentsState, fromApartments.getSwitchPrice);
export const getTags = createSelector(getApartmentsState, fromApartments.getTags);
export const getApartmentsLoaded = createSelector(getApartmentsState, fromApartments.getApartmentsLoaded);
export const getApartmentsLoading = createSelector(getApartmentsState, fromApartments.getApartmentsLoading);
