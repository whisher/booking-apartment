import { createSelector } from '@ngrx/store';

import * as fromRoot from '@app/store/reducers';
import * as fromFeature from '../reducers';
import * as fromApartmentDetails from '../reducers/details.reducer';


export const getApartmentDetailsState = createSelector(
  fromFeature.getPropertiesState,
  (state: fromFeature.PropertiesState) => state.apartment
);

export const getApartmentDetails = createSelector(getApartmentDetailsState, fromApartmentDetails.getApartmentDetails);
export const getApartmentDetailsLoaded = createSelector(getApartmentDetailsState, fromApartmentDetails.getApartmentDetailsLoaded);
export const getApartmentDetailsLoading = createSelector(getApartmentDetailsState, fromApartmentDetails.getApartmentDetailsLoading);
