import { Action } from '@ngrx/store';

import {
  Apartment
} from '../../models/entity.model';

export const APARTMENT_DETAILS = '[properties] apartment details';
export const APARTMENT_DETAILS_FAIL = '[properties] apartment details fail';
export const APARTMENT_DETAILS_SUCCESS = '[properties] apartment details success';

export class ApartmentDetails implements Action {
  readonly type = APARTMENT_DETAILS;
  constructor(public payload: { lang, id, querykey }) { }
}
export class ApartmentDetailsFail implements Action {
  readonly type = APARTMENT_DETAILS_FAIL;
  constructor(public payload: any) { }
}
export class ApartmentDetailsSuccess implements Action {
  readonly type = APARTMENT_DETAILS_SUCCESS;
  constructor(public payload: Apartment) { }
}

export type ApartmentDetailsActions = ApartmentDetails | ApartmentDetailsFail | ApartmentDetailsSuccess;
