import { Action } from '@ngrx/store';

import {
  ApartmentsSearchParams
} from '../../models/search.model';

import {
  ApartmentsLoadParams
} from '../../models/load.model';

import {
  ApartmentsEntity
} from '../../models/entity.model';

export const APARTMENTS_SEARCH = '[properties] apartments search';
export const APARTMENTS_SEARCH_FAIL = '[properties] apartments search fail';
export const APARTMENTS_SEARCH_SUCCESS = '[properties] apartments search success';

export const APARTMENTS_LOAD = '[properties] apartments load';
export const APARTMENTS_LOAD_FAIL = '[properties] apartments load fail';
export const APARTMENTS_LOAD_SUCCESS = '[properties] apartments load success';

export const APARTMENTS_REFRESH = '[properties] apartments refresh';
export const APARTMENTS_REFRESH_FAIL = '[properties] apartments rrefresh success';
export const APARTMENTS_REFRESH_SUCCESS = '[properties] apartments refresh success';

export class ApartmentsSearch implements Action {
  readonly type = APARTMENTS_SEARCH;
  constructor(public payload: ApartmentsSearchParams) { }
}
export class ApartmentsSearchFail implements Action {
  readonly type = APARTMENTS_SEARCH_FAIL;
  constructor(public payload: any) { }
}
export class ApartmentsSearchSuccess implements Action {
  readonly type = APARTMENTS_SEARCH_SUCCESS;
  constructor(public payload: ApartmentsEntity) { }
}
export class ApartmentsLoad implements Action {
  readonly type = APARTMENTS_LOAD;
  constructor(public payload: ApartmentsLoadParams) { }
}
export class ApartmentsLoadFail implements Action {
  readonly type = APARTMENTS_LOAD_FAIL;
  constructor(public payload: any) { }
}
export class ApartmentsLoadSuccess implements Action {
  readonly type = APARTMENTS_LOAD_SUCCESS;
  constructor(public payload: ApartmentsEntity) { }
}
export class ApartmentsRefresh implements Action {
  readonly type = APARTMENTS_REFRESH;
  constructor(public payload: { lang: string, querykey: string, pageSize: number }) { }
}
export class ApartmentsRefreshFail implements Action {
  readonly type = APARTMENTS_REFRESH_FAIL;
  constructor(public payload: any) { }
}
export class ApartmentsRefreshSuccess implements Action {
  readonly type = APARTMENTS_REFRESH_SUCCESS;
  constructor(public payload: ApartmentsEntity) { }
}

export type ApartmentsEntityActions =
  ApartmentsSearch |
  ApartmentsSearchFail |
  ApartmentsSearchSuccess |
  ApartmentsLoad |
  ApartmentsLoadFail |
  ApartmentsLoadSuccess |
  ApartmentsRefresh |
  ApartmentsRefreshFail |
  ApartmentsRefreshSuccess;
