import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';

import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

import {
  ApartmentsSearchParams
} from '../../models/search.model';

import {
  ApartmentsEntity
} from '../../models/entity.model';

import * as fromRoot from '@app/store';
import * as fromApartmentsEntityActions from '../actions/entity.actions';
import { APARTMENTS_ENTITY_SERVICE } from '../../tokens';


@Injectable()
export class ApartmentsEntityEffects {
  @Effect()
  searchApartments$ = this.actions$.ofType(fromApartmentsEntityActions.APARTMENTS_SEARCH)
    .pipe(
    map((action: fromApartmentsEntityActions.ApartmentsSearch) => action.payload),
    switchMap((params: ApartmentsSearchParams) => {
      return this.apartmentsService.search(params)
        .pipe(
        map((entity: ApartmentsEntity) => {
          return { type: fromApartmentsEntityActions.APARTMENTS_SEARCH_SUCCESS, payload: entity };
        }),
        catchError(() => {
          return of({ type: fromApartmentsEntityActions.APARTMENTS_SEARCH_FAIL });
        })
        )
    })
    );

  @Effect()
  searchApartmentsSuccess$ = this.actions$
    .ofType(fromApartmentsEntityActions.APARTMENTS_SEARCH_SUCCESS)
    .pipe(
    map((action: fromApartmentsEntityActions.ApartmentsSearchSuccess) => action.payload),
    map((entity: ApartmentsEntity) => {
      const { lang, querykey, pageSize } = entity.paginationInfo;
      return new fromRoot.Go({
        path: ['/main/apartments/search', lang, querykey, pageSize],
      });
    })
    );

  @Effect()
  loadApartments$ = this.actions$.ofType(fromApartmentsEntityActions.APARTMENTS_LOAD)
    .pipe(
    mergeMap((action: fromApartmentsEntityActions.ApartmentsLoad) => {
      return this.apartmentsService.load(action.payload)
        .pipe(
        map((entity: ApartmentsEntity) => {
          return { type: fromApartmentsEntityActions.APARTMENTS_LOAD_SUCCESS, payload: entity };
        }),
        catchError(() => {
          return of({ type: fromApartmentsEntityActions.APARTMENTS_LOAD_FAIL });
        })
        )
    })
    );

  @Effect()
  loadApartmentsSuccess$ = this.actions$
    .ofType(fromApartmentsEntityActions.APARTMENTS_LOAD_SUCCESS)
    .pipe(
    map((action: fromApartmentsEntityActions.ApartmentsLoadSuccess) => action.payload),
    map((entity: ApartmentsEntity) => {
      const { lang, querykey, pageSize } = entity.paginationInfo;
      return new fromRoot.Go({
        path: ['/main/apartments/search', lang, querykey, pageSize],
      });
    })
    );
  @Effect()
  refreshApartments$ = this.actions$.ofType(fromApartmentsEntityActions.APARTMENTS_REFRESH)
    .pipe(
    mergeMap((action: fromApartmentsEntityActions.ApartmentsRefresh) => {
      const { lang, querykey, pageSize } = action.payload;
      return this.apartmentsService.refresh(lang, querykey, pageSize)
        .pipe(
        map((entity: ApartmentsEntity) => {
          return { type: fromApartmentsEntityActions.APARTMENTS_REFRESH_SUCCESS, payload: entity };
        }),
        catchError(() => {
          return of({ type: fromApartmentsEntityActions.APARTMENTS_REFRESH_FAIL });
        })
        )
    })
    );

  @Effect()
  refreshApartmentsSuccess$ = this.actions$
    .ofType(fromApartmentsEntityActions.APARTMENTS_REFRESH_SUCCESS)
    .pipe(
    map((action: fromApartmentsEntityActions.ApartmentsRefreshSuccess) => action.payload),
    map((entity: ApartmentsEntity) => {
      const { lang, querykey, pageSize } = entity.paginationInfo;
      return new fromRoot.Go({
        path: ['/main/apartments/search', lang, querykey, pageSize],
      });
    })
    );
  constructor(
    private actions$: Actions,
    private router: Router,
    @Inject(APARTMENTS_ENTITY_SERVICE) private apartmentsService
  ) { }
}
