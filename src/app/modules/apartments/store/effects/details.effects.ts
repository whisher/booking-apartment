import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Router } from '@angular/router';

import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';


import {
  Apartment
} from '../../models/entity.model';

import * as fromRoot from '@app/store';
import * as fromApartmentDetailsActions from '../actions/details.actions';
import { APARTMENTS_DETAILS_SERVICE } from '../../tokens';


@Injectable()
export class ApartmentDetailsEffects {
  @Effect()
  apartmentDetails$ = this.actions$.ofType(fromApartmentDetailsActions.APARTMENT_DETAILS)
    .pipe(
    mergeMap((action: fromApartmentDetailsActions.ApartmentDetails) => {
      return this.apartmentService.getById(action.payload.id, action.payload.querykey)
        .pipe(
        map((apartment: Apartment) => {
          const { id } = apartment;
          const lang = 'en';
          const querykey = action.payload.querykey;
          this.router.navigate(['/main/apartments/details', lang, id, querykey]);
          return { type: fromApartmentDetailsActions.APARTMENT_DETAILS_SUCCESS, payload: apartment };
        }),
        catchError(() => {
          return of({ type: fromApartmentDetailsActions.APARTMENT_DETAILS_FAIL });
        })
        )
    })
    );
  /*
  @Effect()
  apartmentDetailsSuccess$ = this.actions$
    .ofType(fromApartmentDetailsActions.APARTMENT_DETAILS_SUCCESS)
    .pipe(
    map((action: fromApartmentDetailsActions.ApartmentDetailsSuccess) => action.payload),
    map((apartment: Apartment) => {
      const { id } = apartment;
      const lang = 'en';
      const querykey = action.querykey;
      return new fromRoot.Go({
        path: ['/main/apartments/details', lang, querykey, pageSize],
      });
    })
  );*/
  constructor(
    private actions$: Actions,
    private router: Router,
    @Inject(APARTMENTS_DETAILS_SERVICE) private apartmentService
  ) { }
}
