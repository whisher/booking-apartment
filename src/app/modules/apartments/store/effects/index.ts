import { ApartmentsEntityEffects } from './entity.effects';
import { ApartmentDetailsEffects } from './details.effects';
export const effects: any[] = [ApartmentsEntityEffects, ApartmentDetailsEffects];

export * from './entity.effects';
export * from './details.effects';
