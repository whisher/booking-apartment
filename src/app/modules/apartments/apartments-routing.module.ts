import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import * as fromContainers from './containers';
import * as fromGuards from './guards';
import { APARTMENTS_GRID_RESOLVER } from './tokens';

const apartmentsRoutes: Routes = [
  {
    path: '', component: fromContainers.ApartmentsComponent,
    children: [
      {
        path: 'search', component: fromContainers.ApartmentsSearchComponent,
        children: [
          {
            path: ':lang/:querykey/:pagesize', component: fromContainers.ApartmentsGridComponent, canActivate: [fromGuards.ApartmentsEntityGuard]
          },
        ]
      },
      {
        path: 'details/:lang/:apartmentId/:querykey', component: fromContainers.ApartmentsDetailsComponent, canActivate: [fromGuards.ApartmentsDetailsGuard]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(apartmentsRoutes)
  ],
  exports: [RouterModule]
})
export class ApartmentsRoutingModule { }
