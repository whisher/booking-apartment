import { Component, OnInit, Inject } from '@angular/core';

@Component({
  selector: 'tnos-apartments',
  templateUrl: './apartments.component.html',
  styleUrls: ['./apartments.component.scss']
})
export class ApartmentsComponent implements OnInit {

  constructor( @Inject('moment') private moment) {
    console.log('date', moment());
  }

  ngOnInit() {
  }

}
