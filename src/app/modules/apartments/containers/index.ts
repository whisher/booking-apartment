import { ApartmentsComponent } from './apartments.component';
import { ApartmentsDetailsComponent } from './apartments-details/apartments-details.component';
import { ApartmentsGridComponent } from './apartments-grid/apartments-grid.component';
import { ApartmentsSearchComponent } from './apartments-search/apartments-search.component';

export const containers: any[] = [
  ApartmentsComponent,
  ApartmentsDetailsComponent,
  ApartmentsGridComponent,
  ApartmentsSearchComponent
];

export * from './apartments.component';
export * from './apartments-details/apartments-details.component';
export * from './apartments-grid/apartments-grid.component';
export * from './apartments-search/apartments-search.component';
