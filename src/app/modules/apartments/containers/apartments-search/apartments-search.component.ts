import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';



import * as fromStore from '../../store';


@Component({
  selector: 'tnos-apartments-search',
  templateUrl: './apartments-search.component.html',
  styleUrls: ['./apartments-search.component.scss']
})
export class ApartmentsSearchComponent implements OnInit {


  constructor(
    private store: Store<fromStore.PropertiesState>
  ) {

  }

  ngOnInit() {

  }

  doSearch() {
    //this.store.dispatch(new fromStore.ApartmentDetails({ id: 3116, querykey: 'V8ZRQBOj0ATCkcdfSq5X' }));
    const data = {
      attributeValues: [
        {
          id: 0,
          label: 'Search city',
          name: 'city_vXKKD',
          value: {
            id: 1,
            name: 'Milan',
            lat: 45.46754,
            lng: 9.1838
          },
          tag: {
            label: 'Search city',
            value: 'Milan'
          }
        },
        {
          id: 4,
          label: 'From',
          name: 'from',
          value: '2017-12-31',
          tag: {
            label: 'From',
            value: '2017-12-31'
          }
        },
        {
          id: 5,
          label: 'To',
          name: 'to',
          value: '2018-01-30',
          tag: {
            label: 'To',
            value: '2018-01-30'
          }
        }
      ],
      filterValues: [],
      idProductcategory: 59,
      language: 'en',
      orderInfo: {
        price: 'ASC',
        name: null
      },
      page: 1,
      pageSize: 10,
      areas: [],
      pois: []
    };
    this.store.dispatch(new fromStore.ApartmentsSearch(data));
  }
}
