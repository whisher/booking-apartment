import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentsSearchComponent } from './apartments-search.component';

describe('HomeComponent', () => {
  let component: ApartmentsSearchComponent;
  let fixture: ComponentFixture<ApartmentsSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApartmentsSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
