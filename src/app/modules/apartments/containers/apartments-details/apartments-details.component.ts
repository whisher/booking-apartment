import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import {
  Apartment
} from '../../models/entity.model';
import * as fromStore from '../../store';
@Component({
  selector: 'tnos-apartments-details',
  templateUrl: './apartments-details.component.html',
  styleUrls: ['./apartments-details.component.scss']
})
export class ApartmentsDetailsComponent implements OnInit {
  isLoading$: Observable<boolean>;
  apartment$: Observable<Apartment>;
  constructor(private store: Store<fromStore.PropertiesState>) { }

  ngOnInit() {
    this.isLoading$ = this.store.select(fromStore.getApartmentDetailsLoading);
    this.apartment$ = this.store.select(fromStore.getApartmentDetails);
    //this.isLoading$.subscribe(console.log);
    //this.apartment$.subscribe(console.log);
  }

}
