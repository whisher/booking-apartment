import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApartmentsGridComponent } from './apartments-grid.component';

describe('GridComponent', () => {
  let component: ApartmentsGridComponent;
  let fixture: ComponentFixture<ApartmentsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApartmentsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApartmentsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
