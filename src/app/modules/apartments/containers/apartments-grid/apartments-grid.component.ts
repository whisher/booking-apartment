import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { DataSource } from '@angular/cdk/collections';

import { Observable } from 'rxjs/Observable';
import { filter } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as fromStore from '../../store';
import { Apartment, PaginationInfo } from '../../models/entity.model';
import { ApartmentsLoadParams } from '../../models/load.model';

@Component({
  selector: 'tnos-apartments-grid',
  templateUrl: './apartments-grid.component.html',
  styleUrls: ['./apartments-grid.component.scss']
})



export class ApartmentsGridComponent implements OnInit, AfterViewInit {
  isLoading$: Observable<boolean>;
  info$: Observable<PaginationInfo>;
  items$: Observable<Apartment[]>;

  length = 100;
  pageSize = 10;
  pageSizeOptions = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;


  constructor(
    private store: Store<fromStore.PropertiesState>
  ) { }

  ngOnInit() {
    this.isLoading$ = this.store.select(fromStore.getApartmentsLoading);
    this.info$ = this.store.select(fromStore.getPaginationInfo);
    this.items$ = this.store.select(fromStore.getApartments);
  }

  refresh() {
    const data: ApartmentsLoadParams = {
      filter: {
        bookingOnLine: false,
        corporate: false,
        nameOrId: null,
        pois: [],
        areas: []
      },
      language: "en",
      orderInfo: {
        price: "ASC",
        name: null
      },
      page: 2,
      pageSize: 10,
      queryKey: "cx1ZdAuI9qwotKwWuQf1"
    };
    this.store.dispatch(new fromStore.ApartmentsLoad(data));
  }
  onUpdateFilter(info: ApartmentsLoadParams) {
    this.store.dispatch(new fromStore.ApartmentsLoad(info))
  }
  onPaginator(currentPage) {
    console.log('currentPage', currentPage);
  }
  goDetails(apartmentId) {
    this.store.select(fromStore.getQuerykey)
      .subscribe(querykey => {
        const data = {
          lang: 'en',
          id: apartmentId,
          querykey
        };
        this.store.dispatch(new fromStore.ApartmentDetails(data));
      });
  }

  ngAfterViewInit() {


  }


}
