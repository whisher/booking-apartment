import { ApartmentsDetailsGuard } from './details.guard';
import { ApartmentsEntityGuard } from './entity.guard';

export const guards: any[] = [ApartmentsDetailsGuard, ApartmentsEntityGuard];

export * from './details.guard';
export * from './entity.guard';
