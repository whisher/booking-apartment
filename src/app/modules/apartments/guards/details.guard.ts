import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { tap, filter, take, switchMap, catchError } from 'rxjs/operators';

import * as fromStore from '../store';

@Injectable()
export class ApartmentsDetailsGuard implements CanActivate {
  constructor(private store: Store<fromStore.PropertiesState>) { }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.checkStore(route).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(route: ActivatedRouteSnapshot): Observable<boolean> {
    return this.store.select(fromStore.getApartmentDetailsLoaded)
      .pipe(
      tap(loaded => {
        if (!loaded) {
          const { lang, apartmentId, querykey } = route.params;
          this.store.dispatch(new fromStore.ApartmentDetails({
            lang: lang,
            id: apartmentId,
            querykey: querykey,
          }));
        }
      }),
      filter(loaded => loaded),
      take(1)
      );
  }
}
