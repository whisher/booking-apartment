import { TestBed, inject } from '@angular/core/testing';

import { ApartmentsEntityService } from './entity.service';

describe('ApartmentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApartmentsEntityService]
    });
  });

  it('should be created', inject([ApartmentsEntityService], (service: ApartmentsEntityService) => {
    expect(service).toBeTruthy();
  }));
});
