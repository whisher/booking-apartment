import { APARTMENTS_DETAILS_SERVICE, APARTMENTS_ENTITY_SERVICE, APARTMENTS_GRID_RESOLVER } from '../tokens';
import { ApartmentDetailsService } from './details.service';
import { ApartmentsEntityService } from './entity.service';

export const services: any[] = [
  { provide: APARTMENTS_DETAILS_SERVICE, useClass: ApartmentDetailsService },
  { provide: APARTMENTS_ENTITY_SERVICE, useClass: ApartmentsEntityService }
];
