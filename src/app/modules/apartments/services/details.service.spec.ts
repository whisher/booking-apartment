import { TestBed, inject } from '@angular/core/testing';

import { ApartmentDetailsService } from './details.service';

describe('ApartmentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApartmentDetailsService]
    });
  });

  it('should be created', inject([ApartmentDetailsService], (service: ApartmentDetailsService) => {
    expect(service).toBeTruthy();
  }));
});
