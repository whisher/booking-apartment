import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';

import {
  Apartment
} from '../models/entity.model';


@Injectable()
export class ApartmentDetailsService {
  constructor(
    private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi) {
  }

  getById(id, querykey): Observable<Apartment> {
    const url = this.urls.apartments.getById('en', id, querykey);
    return this.http.get<Apartment>(url)
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }
}
