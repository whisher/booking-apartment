import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';

import {
  ApartmentsSearchParams
} from '../models/search.model';
import {
  ApartmentsLoadParams
} from '../models/load.model';
import {
  ApartmentsEntity
} from '../models/entity.model';

@Injectable()
export class ApartmentsEntityService {

  urlSearch: string;
  urlLoad: string;
  urlRefresh: (lang, querykey, pageSize) => {};
  constructor(
    private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi) {
    this.urlSearch = urls.apartments.search;
    this.urlLoad = urls.apartments.load;

  }

  search(data: ApartmentsSearchParams): Observable<ApartmentsEntity> {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<ApartmentsEntity>(this.urlSearch, JSON.stringify(data), { headers })
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }

  load(data: ApartmentsLoadParams): Observable<ApartmentsEntity> {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<ApartmentsEntity>(this.urlLoad, JSON.stringify(data), { headers })
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }

  refresh(lang, querykey, pageSize): Observable<ApartmentsEntity> {
    const url = this.urls.apartments.refresh(lang, querykey, pageSize);
    return this.http.get<ApartmentsEntity>(url)
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }
}
