import { Component } from '@angular/core';

import { routerTransition } from './app.animations';

@Component({
  selector: 'tnos-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routerTransition()]
})
export class AppComponent {
  constructor() {
    //throw new Error('Im errorn')
  }
  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
