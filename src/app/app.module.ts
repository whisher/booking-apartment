import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { TranslateModule } from '@ngx-translate/core';

import * as moment from 'moment';

import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { localStorageSync } from 'ngrx-store-localstorage';

// not used in production
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { storeFreeze } from 'ngrx-store-freeze';

const customStorage: Storage = {
  length: 0,
  clear: function(): void {
    if (window && window.localStorage) {
      window.localStorage.clear();
      this.length = window.localStorage.length;
    }
  },
  getItem: function(key: string): string | null {
    try {
      return window.localStorage.getItem(key);
    } catch (err) {
      return null;
    }
  },
  key: function(index: number): string | null {
    try {
      return window.localStorage.key(index);
    } catch (err) {
      return null;
    }
  },
  removeItem: function(key: string): void {
    try {
      window.localStorage.removeItem(key);
      this.length = window.localStorage.length;
    } catch (err) {
      return;
    }
  },
  setItem: function(key: string, data: string): void {
    try {
      window.localStorage.setItem(key, data);
      this.length = window.localStorage.length;
    } catch (err) {
      return;
    }
  }
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync(
    {
      keys: ['authentication'],
      rehydrate: true,
      storage: customStorage
    })(reducer);
}

const metaReducersList = [localStorageSyncReducer];
if (!environment.production) {
  metaReducersList.push(storeFreeze)
}

export const metaReducers: MetaReducer<any>[] = metaReducersList;

import { reducers, effects, CustomSerializer } from './store';

import { effectsAuthentication } from './core/authentication/store/effects';

import { GlobalErrorHandler } from './app.errorhandler';
import { API_URLS } from './app.config';
import { API_URLS_TOKEN } from './app.tokens';
import { AppPreloading } from './app.preloading';
import { AppRoutingModule } from './app-routing.module';
import { LoggingModule } from './core/logging/logging.module';
import { AuthenticationModule } from './core/authentication/authentication.module';
import { OneColumnModule } from './core/layouts/one-column/one-column.module';
import { PagesModule } from './pages/pages.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 't-nos' }),
    BrowserTransferStateModule,
    BrowserAnimationsModule,
    HttpClientModule,
    //TranslateModule.forRoot(),
    AppRoutingModule,
    LoggingModule,
    AuthenticationModule,
    OneColumnModule,
    PagesModule,
    StoreModule.forRoot(
      reducers,
      { metaReducers }
    ),
    EffectsModule.forRoot([...effects, ...effectsAuthentication]),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [
    {
      provide: 'moment', useFactory: (): any => moment
    },
    { provide: RouterStateSerializer, useClass: CustomSerializer },
    AppPreloading,
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler
    },
    { provide: API_URLS_TOKEN, useValue: API_URLS },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
