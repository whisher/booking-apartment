import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { routerTransition } from '../../../../app.animations';

@Component({
  selector: 'tnos-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  animations: [ routerTransition() ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageComponent implements OnInit {

  constructor() { }
  ngOnInit() {}

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }
}
