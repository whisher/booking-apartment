import { NgModule, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';

import {
  FooterComponent,
  HeaderComponent,
  HeaderBarComponent,
  HeaderBrandComponent,
  HeaderMastheadComponent,
  HeaderNavComponent,
  MainComponent,
  PageComponent
} from './';

import {
  CONTACTS_RESOLVER,
  CONTACTS_SERVICE,
  ContactsComponent,
  ContactsResolve,
  ContactsService,
  LANGUAGES_RESOLVER,
  LANGUAGES_SERVICE,
  LanguagesComponent,
  LanguagesResolve,
  LanguagesService
} from '../components/';


@NgModule({
  imports: [
    RouterModule,
    SharedModule
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    HeaderBarComponent,
    HeaderBrandComponent,
    HeaderMastheadComponent,
    HeaderNavComponent,
    MainComponent,
    PageComponent,
    LanguagesComponent,
    ContactsComponent,
    LanguagesComponent
  ],
  providers: [
    { provide: CONTACTS_SERVICE, useClass: ContactsService },
    { provide: LANGUAGES_SERVICE, useClass: LanguagesService },
    { provide: CONTACTS_RESOLVER, useClass: ContactsResolve },
    { provide: LANGUAGES_RESOLVER, useClass: LanguagesResolve }
  ]
})
export class OneColumnModule {
  constructor(
    @Optional() @SkipSelf() parentModule: OneColumnModule
  ) {
    if (parentModule) {
      throw new Error('OneColumnModule is already loaded. Import only in AppModule');
    }
  }
}
