import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';

import {
  AuthenticationAccount
} from '@app/core/authentication/models/account.model';

import * as fromRootStore from '@app/store';
import * as fromStore from '@app/core/authentication/store';

import {
  Contact,
  Language,
}
  from '@app/core/layouts/components/';

@Component({
  selector: 'tnos-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {
  account$: Observable<AuthenticationAccount>;
  contacts: Contact[];
  languages: Language[];
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<fromRootStore.AppState>,

  ) { }

  ngOnInit() {
    this.account$ = this.store.select(fromStore.getAccount);
    this.contacts = this.route.snapshot.data['contacts'];
    this.languages = this.route.snapshot.data['languages'];
  }

  onSelectLanguage(event) {
    alert(event);
  }

  logout(): void {
    this.router.navigateByUrl('/logout');
  }
}
