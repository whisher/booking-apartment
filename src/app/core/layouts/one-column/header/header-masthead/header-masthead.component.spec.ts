import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderMastheadComponent } from './header-masthead.component';

describe('HeaderMastheadComponent', () => {
  let component: HeaderMastheadComponent;
  let fixture: ComponentFixture<HeaderMastheadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderMastheadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderMastheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
