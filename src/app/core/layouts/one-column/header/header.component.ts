import {
  Component
} from '@angular/core';

@Component({
  selector: 'tnos-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  constructor() { }
}
