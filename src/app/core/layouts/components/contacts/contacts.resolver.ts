import { Injectable, Inject } from '@angular/core';
import { Resolve } from '@angular/router';

import { Contact } from './contact.model';
import { CONTACTS_SERVICE } from './contacts.tokens';

@Injectable()
export class ContactsResolve implements Resolve<Contact[]> {
  constructor( @Inject(CONTACTS_SERVICE) private contactsService)
  { }
  resolve() {
    return this.contactsService.get();
  }
}
