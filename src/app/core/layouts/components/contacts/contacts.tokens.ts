import { InjectionToken } from '@angular/core';

export const CONTACTS_SERVICE = new InjectionToken('contacts.service');
export const CONTACTS_RESOLVER = new InjectionToken('contacts.resolver');
