import { async, fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ChangeDetectionStrategy } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { Contact } from './contact.model';
import { ContactsComponent } from './contacts.component';

const contacts: Contact[] = [{
  type: "",
  contact: "Skype: Tech-Nos",
  ico: ""
}, {
  type: "",
  contact: "info@tech-nos.com",
  ico: ""
}];

describe('ContactsComponent', () => {
  let component: ContactsComponent;
  let debugEl: DebugElement;
  let element: HTMLElement;
  let fixture: ComponentFixture<ContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FormsModule],
      declarations: [ContactsComponent]
    })
      .compileComponents();
    fixture = TestBed.overrideComponent(ContactsComponent, {
      set: {
        selector: 'tnos-contacts',
        templateUrl: './contacts.component.html',
        changeDetection: ChangeDetectionStrategy.Default
      }
    })
      .createComponent(ContactsComponent);

    fixture.detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    element = debugEl.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show `` as option', async(() => {
    component.contacts = contacts;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const options = debugEl.queryAll(By.css('select option'));
    });
  }));
});
