import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Contact } from './contact.model';

@Component({
  selector: 'tnos-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactsComponent {
  @Input() contacts: Contact[];
  constructor() { }
  onSelect(contact: Contact) { }
}
