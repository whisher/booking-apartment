export interface Contact {
  contact: string;
  ico: string;
  type: string;
}
