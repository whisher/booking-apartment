import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { map, catchError } from 'rxjs/operators';

import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';

import { Contact } from './contact.model';

@Injectable()
export class ContactsService {

  urlContact: string;

  constructor(
    private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi
  ) {
    this.urlContact = urls.contact('en');
  }

  get(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.urlContact)
      .pipe(
      map(data => {
        return data['data'];
      }),
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      }));
  }
}
