import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';
import { Language } from './language.model';

@Injectable()
export class LanguagesService {

  urlLanguage: string;

  constructor(
    private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi
  ) {
    this.urlLanguage = urls.language('en');
  }

  get(): Observable<Language[]> {
    return this.http.get<Language[]>(this.urlLanguage)
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }

}
