import { InjectionToken } from '@angular/core';

export const LANGUAGES_SERVICE = new InjectionToken('languages.service');
export const LANGUAGES_RESOLVER = new InjectionToken('languages.resolver');
