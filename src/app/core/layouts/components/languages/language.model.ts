export interface Language {
  default: boolean;
  id: number;
  label: string;
  locale: string;
}
