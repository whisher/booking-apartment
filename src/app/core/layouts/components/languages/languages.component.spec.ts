import { async, fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement, ChangeDetectionStrategy } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { LanguagesComponent } from './languages.component';
import { Language } from './language.model';

const languages: Language[] = [{
  id: 1,
  label: 'Italiano',
  locale: 'it',
  default: false
}, {
  id: 2,
  label: 'English',
  locale: 'en',
  default: true
}];

describe('LanguagesComponent', () => {
  let component: LanguagesComponent;
  let debugEl: DebugElement;
  let element: HTMLElement;
  let fixture: ComponentFixture<LanguagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserModule, FormsModule],
      declarations: [LanguagesComponent]
    })

      .overrideComponent(LanguagesComponent, {
        set: {
          selector: 'tnos-languages',
          templateUrl: './languages.component.html',
          changeDetection: ChangeDetectionStrategy.Default
        }
      })
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguagesComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    element = debugEl.nativeElement;
    component.languages = languages;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show `Italiano English` as option', () => {
    fixture.detectChanges();
    const options = debugEl.queryAll(By.css('select option'));
    expect(options[0].nativeElement.text).toBe('Italiano');
    expect(options[1].nativeElement.text).toBe('English');
  });

  it('should show `Italiano English` as option', () => {
    spyOn(component, 'onSelect');
    const select = debugEl.query(By.css('select')).nativeElement;
    select.value = select.options[1].value;
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(component.onSelect).toHaveBeenCalled();
    expect(component.onSelect).toHaveBeenCalledWith('en');
  });

  it('should emit on change', () => {
    spyOn(component.change, 'emit');
    const select = debugEl.query(By.css('select')).nativeElement;
    select.value = select.options[1].value;
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(component.change.emit).toHaveBeenCalled();
    expect(component.change.emit).toHaveBeenCalledWith('en');
  });
});
