import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Language } from './language.model';

@Component({
  selector: 'tnos-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LanguagesComponent {
  @Input() languages: Language[];
  @Output()
  change: EventEmitter<string> = new EventEmitter<string>();
  constructor() { }
  onSelect(locale: string) {
    this.change.emit(locale);
  }
}
