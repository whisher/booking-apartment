import { Injectable, Inject } from '@angular/core';
import { Resolve } from '@angular/router';

import { Language } from './language.model';
import { LANGUAGES_SERVICE } from './languages.tokens';

@Injectable()
export class LanguagesResolve implements Resolve<Language[]> {
  constructor( @Inject(LANGUAGES_SERVICE) private languagesService) { }
  resolve() {
    return this.languagesService.get();
  }
}
