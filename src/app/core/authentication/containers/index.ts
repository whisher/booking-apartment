import { AuthenticationLoginComponent } from './authentication-login/authentication-login.component';
import { AuthenticationLogoutComponent } from './authentication-logout/authentication-logout.component';

export const containers: any[] = [
  AuthenticationLoginComponent,
  AuthenticationLogoutComponent
];

export * from './authentication-login/authentication-login.component';
export * from './authentication-logout/authentication-logout.component';
