import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as fromRootStore from '@app/store';
import * as fromStore from '../../store';

@Component({
  selector: 'tnos-login',
  templateUrl: './authentication-login.component.html',
  styleUrls: ['./authentication-login.component.scss']
})
export class AuthenticationLoginComponent {
  showBtn = true;
  isLoading$: Observable<boolean>;
  hasError$: Observable<boolean>;
  @ViewChild('f') loginForm: NgForm;

  constructor(private store: Store<fromRootStore.AppState>) { }
  ngOnInit() {
    this.isLoading$ = this.store.select(fromStore.getAuthenticationLoading);
    this.hasError$ = this.store.select(fromStore.getAuthenticationError);
  }
  onSubmit(): void {
    this.store.dispatch(new fromStore.Login(this.loginForm.value));
  }

  showPassword(input: any): any {
    input.type = input.type === 'password' ? 'text' : 'password';
  }

}
