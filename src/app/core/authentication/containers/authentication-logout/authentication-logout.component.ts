import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';

import * as fromRootStore from '@app/store';
import * as fromStore from '../../store';

@Component({
  selector: 'tnos-authentication-logout',
  templateUrl: './authentication-logout.component.html',
  styleUrls: ['./authentication-logout.component.scss']
})
export class AuthenticationLogoutComponent implements OnInit {

  constructor(
    private router: Router,
    private store: Store<fromRootStore.AppState>)
  { }

  ngOnInit() {
    this.store.dispatch(new fromStore.Logout());
  }

}
