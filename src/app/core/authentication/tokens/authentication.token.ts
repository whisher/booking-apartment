import { InjectionToken } from '@angular/core';

export const AUTHENTICATION_SERVICE = new InjectionToken('authentication.service');
