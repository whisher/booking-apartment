import { InjectionToken } from '@angular/core';

export const ACCOUNT_SERVICE = new InjectionToken('account.service');
