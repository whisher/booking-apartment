import { StoreModule, Store, combineReducers } from '@ngrx/store';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';

import { TestBed } from '@angular/core/testing';


import * as fromRoot from '@app/store';
import * as fromReducers from '../reducers/authentication.reducer';
import * as fromActions from '../actions/authentication.actions';
import * as fromSelectors from '../selectors';

fdescribe('Authentication Selectors', () => {
  let store: Store<fromRoot.AppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          ...fromRoot.reducers
        }),
      ],
    });

    store = TestBed.get(Store);
  });

  describe('get authenticated', () => {
    it('should return state of pizza store slice', () => {
      let result;
      store
        .select(fromSelectors.getAuthentication)
        .subscribe(value => (result = value));

      expect(result).toEqual(false);

      store.dispatch(new fromActions.LoginSuccess());

      expect(result).toEqual(true);
    });
  });

  describe('get error', () => {
    it('should return state of pizza store slice', () => {
      let result;
      const payload = { message: 'Invalid login' };
      store
        .select(fromSelectors.getAccountError)
        .subscribe(value => (result = value));

      expect(result).toEqual(null);

      store.dispatch(new fromActions.LoginFail(payload));

      expect(result).toEqual(payload);
    });
  });


});
