import { createSelector } from '@ngrx/store';

import * as fromStore from '@app/store';

export const getAccount = (state: fromStore.AppState) => state.account.account;
export const getAccountError = (state: fromStore.AppState) => state.account.error;
export const getAccountLoaded = (state: fromStore.AppState) => state.account.loaded;
export const getAccountLoading = (state: fromStore.AppState) => state.account.loading;

export const getAuthentication = (state: fromStore.AppState) => state.authentication.authenticated;
export const getAuthenticationError = (state: fromStore.AppState) => state.authentication.error;
export const getAuthenticationLoading = (state: fromStore.AppState) => state.authentication.loading;
export const getAuthenticationToken = (state: fromStore.AppState) => state.authentication.token;
