import * as fromAuthentication from './authentication.actions';
import {
  AuthenticationToken
} from '../../models/authentication.model';

describe('Authentication Actions', () => {
  it('should create an login action', () => {
    const credentials = { username: 'admin', password: 'admin' };
    const action = new fromAuthentication.Login(credentials);
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGIN,
      payload: credentials
    });
  });

  it('should create an login fail action', () => {
    const payload = { message: 'Invalid login' };
    const action = new fromAuthentication.LoginFail(payload);
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGIN_FAIL,
      payload
    });
  });

  it('should create an login reset action', () => {
    const action = new fromAuthentication.LoginReset();
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGIN_RESET
    });
  });

  it('should create an login success action', () => {
    const action = new fromAuthentication.LoginSuccess();
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGIN_SUCCESS
    });
  });

  it('should create an logout action', () => {
    const action = new fromAuthentication.Logout();
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGOUT
    });
  });

  it('should create an logout fail action', () => {
    const payload = { message: 'Invalid logout' };
    const action = new fromAuthentication.LogoutFail(payload);
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGOUT_FAIL,
      payload
    });
  });

  it('should create an logout success action', () => {
    const action = new fromAuthentication.LogoutSuccess();
    expect({ ...action }).toEqual({
      type: fromAuthentication.LOGOUT_SUCCESS
    });
  });

  it('should create an set token action', () => {
    const token: AuthenticationToken = {
      token: 'server_token',
      expiresIn: 1000,
      expiresAt: 1500
    };
    const action = new fromAuthentication.SetToken(token);
    expect({ ...action }).toEqual({
      type: fromAuthentication.SET_TOKEN,
      payload: token
    });
  });

  it('should create an redirect home action', () => {
    const action = new fromAuthentication.RedirectHome();
    expect({ ...action }).toEqual({
      type: fromAuthentication.REDIRECT_HOME
    });
  });
});
