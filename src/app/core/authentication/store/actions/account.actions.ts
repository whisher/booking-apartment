import { Action } from '@ngrx/store';

import {
  AuthenticationAccount
} from '../../models/account.model';

export const ACCOUNT = '[authentication] account';
export const ACCOUNT_FAIL = '[authentication] account fail';
export const ACCOUNT_RESET = '[authentication] account reset';
export const ACCOUNT_SUCCESS = '[authentication] account success';

export class Account implements Action {
  readonly type = ACCOUNT;
}
export class AccountFail implements Action {
  readonly type = ACCOUNT_FAIL;
  constructor(public payload: any) { }
}
export class AccountReset implements Action {
  readonly type = ACCOUNT_RESET;
}
export class AccountSuccess implements Action {
  readonly type = ACCOUNT_SUCCESS;
  constructor(public payload: AuthenticationAccount) { }
}

export type AccountActions =
  Account |
  AccountFail |
  AccountReset |
  AccountSuccess;
