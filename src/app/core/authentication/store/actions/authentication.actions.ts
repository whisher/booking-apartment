import { Action } from '@ngrx/store';

import {
  AuthenticationToken
} from '../../models/authentication.model';

export const LOGIN = '[authentication] login';
export const LOGIN_FAIL = '[authentication] login fail';
export const LOGIN_RESET = '[authentication] login reset';
export const LOGIN_SUCCESS = '[authentication] login success';


export const LOGOUT = '[authentication] logout';
export const LOGOUT_FAIL = '[authentication] logout fail';
export const LOGOUT_SUCCESS = '[authentication] logout success';

export const SET_TOKEN = '[authentication] set token';

export const REDIRECT_HOME = '[authentication] redirect to home';

export class Login implements Action {
  readonly type = LOGIN;
  constructor(public payload: { username: string, password: string }) { }
}
export class LoginFail implements Action {
  readonly type = LOGIN_FAIL;
  constructor(public payload: any) { }
}
export class LoginReset implements Action {
  readonly type = LOGIN_RESET;
}
export class LoginSuccess implements Action {
  readonly type = LOGIN_SUCCESS;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}
export class LogoutFail implements Action {
  readonly type = LOGOUT_FAIL;
  constructor(public payload: any) { }
}
export class LogoutSuccess implements Action {
  readonly type = LOGOUT_SUCCESS;
}
export class SetToken implements Action {
  readonly type = SET_TOKEN;
  constructor(public payload: AuthenticationToken) { }
}
export class RedirectHome implements Action {
  readonly type = REDIRECT_HOME;
}
export type AuthenticationActions =
  Login |
  LoginFail |
  LoginReset |
  LoginSuccess |
  Logout |
  LogoutFail |
  LogoutSuccess |
  SetToken |
  RedirectHome;
