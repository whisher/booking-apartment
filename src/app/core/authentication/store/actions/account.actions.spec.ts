import * as fromAccount from './account.actions';
import {
  AuthenticationAccount
} from '../../models/account.model';

describe('Account Actions', () => {
  it('should create an account action', () => {
    const action = new fromAccount.Account();
    expect({ ...action }).toEqual({
      type: fromAccount.ACCOUNT
    });
  });

  it('should create an account fail action', () => {
    const payload = { message: 'Invalid account' };
    const action = new fromAccount.AccountFail(payload);
    expect({ ...action }).toEqual({
      type: fromAccount.ACCOUNT_FAIL,
      payload
    });
  });

  it('should create an account reset action', () => {
    const action = new fromAccount.AccountReset();
    expect({ ...action }).toEqual({
      type: fromAccount.ACCOUNT_RESET
    });
  });

  it('should create an account success action', () => {
    const payload = {
      id: 1,
      login: 'admin',
      firstName: 't-nos',
      lastName: '',
      email: null,
      imageUrl: null,
      activated: true,
      authorities: ['ROLE_ADMIN']
    };
    const action = new fromAccount.AccountSuccess(payload);
    expect({ ...action }).toEqual({
      type: fromAccount.ACCOUNT_SUCCESS,
      payload
    });
  });

});
