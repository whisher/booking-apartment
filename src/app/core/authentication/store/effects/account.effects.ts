import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';

import {
  AuthenticationAccount
} from '../../models/account.model';

import * as fromRoot from '@app/store';
import * as fromActions from '../actions';

import { ACCOUNT_SERVICE } from '../../tokens';

@Injectable()
export class AccountEffects {
  @Effect()
  account$ = this.actions$.ofType(fromActions.ACCOUNT)
    .pipe(
    switchMap(() => {
      return this.accountService.account()
        .pipe(
        map((account: AuthenticationAccount) => {
          return { type: fromActions.ACCOUNT_SUCCESS, payload: account };
        }),
        catchError((error) => {
          const actions = [{ type: fromActions.ACCOUNT_FAIL, payload: error }, { type: fromActions.LOGIN_RESET }];
          return from(actions);
        })
        )
    })
    );
  logoutSuccess$ = this.actions$
    .ofType(
    fromActions.ACCOUNT_FAIL,
    fromActions.LOGIN_RESET
    )
    .pipe(
    map(() => {
      return new fromRoot.Go({
        path: ['/home'],
      });
    })
    );
  constructor(
    private actions$: Actions,
    @Inject(ACCOUNT_SERVICE) private accountService
  ) { }
}
