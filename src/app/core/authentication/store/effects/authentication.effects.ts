import { Injectable, Inject } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';

import { catchError, map, mergeMap, switchMap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { from } from 'rxjs/observable/from';

import {
  AuthenticationToken
} from '../../models/authentication.model'

import * as fromRoot from '@app/store';
import * as fromActions from '../actions';

import { AUTHENTICATION_SERVICE } from '../../tokens';

@Injectable()
export class AuthenticationEffects {
  @Effect()
  login$ = this.actions$.ofType(fromActions.LOGIN)
    .pipe(
    map((action: fromActions.Login) => action.payload),
    switchMap((params: { username: string, password: string }) => {
      return this.authenticationService.login(params)
        .pipe(
        mergeMap((token: AuthenticationToken) => {
          return [
            { type: fromActions.LOGIN_SUCCESS },
            { type: fromActions.SET_TOKEN, payload: token }
          ]
        }),
        catchError((error) => {
          return of({ type: fromActions.LOGIN_FAIL, payload: error });
        })
        )
    })
    );

  @Effect()
  loginSuccess$ = this.actions$
    .ofType(fromActions.SET_TOKEN)
    .pipe(
    map(() => {
      return new fromRoot.Go({
        path: ['/main/dashboard'],
      });
    })
    );

  @Effect()
  redirectHome$ = this.actions$
    .ofType(fromActions.REDIRECT_HOME)
    .pipe(
    map(() => {
      return new fromRoot.Go({
        path: ['/home'],
      });
    })
    );
  @Effect()
  logout$ = this.actions$.ofType(fromActions.LOGOUT)
    .pipe(
    switchMap(() => {
      return this.authenticationService.logout()
        .pipe(
        mergeMap((action: any) => {
          return [
            { type: fromActions.LOGOUT_SUCCESS },
            { type: fromActions.ACCOUNT_RESET }
          ]
        }),
        catchError((error) => {
          const actions = [{ type: fromActions.LOGOUT_FAIL, payload: error }, { type: fromActions.ACCOUNT_RESET }];
          return from(actions);
        }))
    })
    );
  @Effect()
  logoutSuccess$ = this.actions$
    .ofType(
    fromActions.LOGOUT_SUCCESS,
    fromActions.LOGOUT_FAIL
    )
    .pipe(
    map(() => {
      return new fromRoot.Go({
        path: ['/home'],
      });
    })
    );
  constructor(
    private actions$: Actions,
    @Inject(AUTHENTICATION_SERVICE) private authenticationService
  ) { }
}
