import { AccountEffects } from './account.effects';
import { AuthenticationEffects } from './authentication.effects';

export const effectsAuthentication: any[] = [
  AccountEffects,
  AuthenticationEffects
];

export * from './authentication.effects';
export * from './account.effects';
