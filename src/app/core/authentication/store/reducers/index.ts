/*import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromAccount from './account.reducer';
import * as fromAuthentication from './authentication.reducer';

export interface AuthenticatorState {
  account: fromAccount.AccountState;
  authentication: fromAuthentication.AuthenticationState;
}

export const reducers: ActionReducerMap<AuthenticatorState> = {
  account: fromAccount.accountReducer,
  authentication: fromAuthentication.authenticationReducer
};

export const getPropertiesState = createFeatureSelector<AuthenticatorState>(
  'authenticator'
);
*/
export * from './authentication.reducer';
export * from './account.reducer';
