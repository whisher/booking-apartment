import * as fromActions from '../actions';

import {
  AuthenticationToken
} from '../../models/authentication.model';

export interface AuthenticationState {
  authenticated: boolean;
  error: any;
  loading: boolean;
  token: AuthenticationToken;
}

export const initialState: AuthenticationState = {
  authenticated: false,
  error: null,
  loading: false,
  token: null
};

export function authenticationReducer(
  state = initialState,
  action: fromActions.AuthenticationActions
): AuthenticationState {
  switch (action.type) {
    case (fromActions.LOGIN):
      return {
        ...state,
        loading: true
      };
    case (fromActions.LOGIN_FAIL):
      return {
        ...state,
        authenticated: false,
        error: action.payload,
        loading: false
      };
    case (fromActions.LOGIN_SUCCESS):
      return {
        ...state,
        authenticated: true,
        error: null,
        loading: false
      };
    case (fromActions.SET_TOKEN):
      return {
        ...state,
        token: action.payload
      };
    case (fromActions.LOGIN_RESET):
      return {
        ...state,
        authenticated: false,
        error: null,
        loading: false
      };
    case (fromActions.LOGOUT):
      return {
        ...state,
        loading: true
      };
    case (fromActions.LOGOUT_FAIL):
      return {
        ...state,
        authenticated: false,
        error: action.payload,
        loading: false,
        token: null
      };
    case (fromActions.LOGOUT_SUCCESS):
      return {
        ...state,
        authenticated: false,
        error: null,
        loading: false,
        token: null
      };
    default:
      return state;
  }
}
