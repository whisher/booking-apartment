import * as fromActions from '../actions/account.actions';
import * as fromReducer from './account.reducer';
import {
  AuthenticationAccount
} from '../../models/account.model';

describe('Authentication Reducer', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const { initialState } = fromReducer;
      const action = {} as any;
      const state = fromReducer.authenticationReducer(undefined, action);
      expect(state).toBe(initialState);
    });
  });
  describe('LOGIN action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromReducer;
      const credentials = { username: 'admin', password: 'admin' }
      const action = new fromActions.Login(credentials);
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(true);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGIN FAIL action', () => {
    it('should set error payload to true', () => {
      const { initialState } = fromReducer;
      const payload = { message: 'Invalid login' };
      const action = new fromActions.LoginFail(payload);
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(payload);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGIN SUCCESS action', () => {
    it('should set authenticated to true', () => {
      const { initialState } = fromReducer;
      const action = new fromActions.LoginSuccess();
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(true);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGIN SET TOKEN action', () => {
    it('should set a token', () => {
      const { initialState } = fromReducer;
      const token: AuthenticationToken = {
        token: 'server_token',
        expiresIn: 1000,
        expiresAt: 1500
      };
      const action = new fromActions.SetToken(token);
      const state = fromReducer.authenticationReducer(initialState, action);
      /* TODO */
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(token);
    });
  });
  describe('LOGIN RESET action', () => {
    it('should set all to initial value', () => {
      const { initialState } = fromReducer;
      const action = new fromActions.LoginReset();
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGOUT action', () => {
    it('should set loading to true', () => {
      const { initialState } = fromReducer;
      const action = new fromActions.Logout();
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(true);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGOUT FAIL action', () => {
    it('should set error payload to true', () => {
      const { initialState } = fromReducer;
      const payload = { message: 'Invalid login' };
      const action = new fromActions.LogoutFail(payload);
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(payload);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(null);
    });
  });
  describe('LOGOUT SUCCESS action', () => {
    it('should set authenticated to false', () => {
      const { initialState } = fromReducer;
      const action = new fromActions.LogoutSuccess();
      const state = fromReducer.authenticationReducer(initialState, action);
      expect(state.authenticated).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.loading).toEqual(false);
      expect(state.token).toEqual(null);
    });
  });
});
