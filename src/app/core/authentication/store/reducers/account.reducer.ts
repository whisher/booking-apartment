import * as fromActions from '../actions';

import {
  AuthenticationAccount
} from '../../models/account.model';

export interface AccountState {
  account: AuthenticationAccount;
  error: any;
  loaded: boolean;
  loading: boolean;
}

const initialState: AccountState = {
  account: {} as AuthenticationAccount,
  error: null,
  loaded: false,
  loading: false
};

export function accountReducer(
  state = initialState,
  action: fromActions.AccountActions
): AccountState {
  switch (action.type) {
    case (fromActions.ACCOUNT):
      return {
        ...state,
        loading: true
      };
    case (fromActions.ACCOUNT_FAIL):
      return {
        ...state,
        account: null,
        error: action.payload,
        loading: false
      };
    case (fromActions.ACCOUNT_SUCCESS):
      return {
        ...state,
        account: action.payload,
        error: null,
        loaded: true,
        loading: false
      };
    default:
      return state;
  }
}
