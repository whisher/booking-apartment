import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { take, map, catchError, filter, switchMap, tap } from 'rxjs/operators';

import { Store } from '@ngrx/store';

import * as fromRootStore from '@app/store';
import * as fromStore from '../store';

@Injectable()
export class AccountGuard implements CanActivate {
  constructor(
    private router: Router,
    private store: Store<fromRootStore.AppState>) {

  }

  canActivate(): Observable<boolean> {
    return this.checkStore().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  checkStore(): Observable<boolean> {
    return this.store.select(fromStore.getAccountLoaded)
      .pipe(
      tap(loaded => {
        if (!loaded) {
          this.store.dispatch(new fromStore.Account());
        }
      }),
      filter(loaded => loaded),
      take(1)
      );
  }
}
