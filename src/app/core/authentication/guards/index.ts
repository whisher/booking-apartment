import { AccountGuard } from './account.guard';
import { AuthenticationGuard } from './authentication.guard';

export const guards: any[] = [AuthenticationGuard, AccountGuard];

export * from './account.guard';
export * from './authentication.guard';
