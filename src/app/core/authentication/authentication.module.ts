import { NgModule, Optional, SkipSelf } from '@angular/core';

import { SharedModule } from '@app/shared/shared.module';

import * as fromContainers from './containers';
import * as fromGuards from './guards';
import * as fromServices from './services';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [...fromContainers.containers],
  providers: [...fromServices.services, ...fromGuards.guards]
})
export class AuthenticationModule {
  constructor(
    @Optional() @SkipSelf() parentModule: AuthenticationModule
  ) {
    if (parentModule) {
      throw new Error('AuthenticationModule is already loaded. Import only in AppModule');
    }
  }
}
