export interface AuthenticationAccount {
  activated: boolean;
  authorities: Array<string>;
  email: string;
  firstName: string;
  id: number;
  lastName: string;
  login: string;
  imageUrl: string;
}
