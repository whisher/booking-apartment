import { HTTP_INTERCEPTORS } from '@angular/common/http';

import {
  ACCOUNT_SERVICE,
  AUTHENTICATION_SERVICE
}
  from '../tokens';
import { AccountService } from './account.service';
import { AuthenticationInterceptor } from './authentication.interceptor';
import { AuthenticationService } from './authentication.service';

export const services: any[] = [
  { provide: ACCOUNT_SERVICE, useClass: AccountService },
  { provide: AUTHENTICATION_SERVICE, useClass: AuthenticationService },
  { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true }
];
