import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';

import {
  AuthenticationToken
} from '../models/authentication.model';

@Injectable()
export class AuthenticationService {
  private clientSecret = 'mySecretOAuthSecret';
  private clientId = 'tnoscoreapp';
  urlLogin: string;
  urlLogout: string;

  constructor(
    private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi) {
    this.urlLogin = urls.authentication.login;
    this.urlLogout = urls.authentication.logout;
  }

  login(credentials): Observable<AuthenticationToken> {
    const data = 'username=' + encodeURIComponent(credentials.username) + '&password=' +
      encodeURIComponent(credentials.password) + '&grant_type=password&scope=read%20write&' +
      'client_secret=' + this.clientSecret + '&client_id=' + this.clientId;

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Accept', 'application/json')
      .set('Authorization', 'Basic ' + this.base64('tnoscoreapp' + ':' + 'mySecretOAuthSecret'));

    return this.http.post(this.urlLogin, data, {
      headers
    })
      .pipe(
      map(this.authSuccess.bind(this)),
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }

  logout(): Observable<any> {
    return this.http.post(this.urlLogout, null)
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }

  private authSuccess(data): AuthenticationToken {
    const expiredAt = new Date();
    console.log('authSuccess', data);
    expiredAt.setSeconds(expiredAt.getSeconds() + data.expires_in);
    const authenticationToken: AuthenticationToken = {
      token: data.access_token,
      expiresIn: data.expires_in,
      expiresAt: expiredAt.getTime()
    };
    return authenticationToken
  }

  private base64(str): string {
    return btoa(str);
  }

}
