import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { AppApi } from '@app/app.api.model';
import { API_URLS_TOKEN } from '@app/app.tokens';

import {
  AuthenticationAccount
} from '../models/account.model';


@Injectable()
export class AccountService {
  urlAccount: string;
  constructor(private http: HttpClient,
    @Inject(API_URLS_TOKEN) private urls: AppApi) {
    this.urlAccount = urls.authentication.account;
  }
  account(): Observable<AuthenticationAccount> {
    return this.http.get<AuthenticationAccount>(this.urlAccount)
      .pipe(
      catchError((error: HttpErrorResponse) => {
        const errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
        return ErrorObservable.create(errorMessage);
      })
      );
  }
  
}
