import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

import { Store } from '@ngrx/store';
import { take, map, switchMap } from 'rxjs/operators';

import {
  AuthenticationToken
} from '../models/authentication.model';
import * as fromRootStore from '@app/store';
import * as fromStore from '../store';

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(
    private store: Store<fromRootStore.AppState>
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(fromStore.getAuthenticationToken)
      .pipe(
      take(1),
      switchMap((token: AuthenticationToken) => {
        if (token) {

          const clonedRequest = request.clone({
            setHeaders: {
              Authorization: `Bearer ${token.token}`
            }
          });
          return next.handle(clonedRequest);
        }
        return next.handle(request);
      })

      );
  }
}
