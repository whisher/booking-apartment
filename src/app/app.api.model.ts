export interface AppApi {
  apartments: {
    search: string;
    load: string;
    refresh(lang, querykey, pagesize): string;
    getById(lang, id, querykey): string;
  },
  authentication: {
    login: string;
    logout: string;
    account: string;
  },
  contact(lang): string,
  language(lang): string
}
