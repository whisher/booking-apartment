import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ErrorComponent } from './error.component';

describe('ErrorComponent', () => {
  let component: ErrorComponent;
  let debugEl: DebugElement;
  let element: HTMLElement;
  let fixture: ComponentFixture<ErrorComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    element = debugEl.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render 404 in a p tag', () => {
    const textContent = debugEl.query(By.css('p')).nativeElement.textContent;
    expect(textContent).toContain('404');
  });

  it('should render Home in a tag', () => {
    const textContent = debugEl.query(By.css('a')).nativeElement.textContent;
    expect(textContent).toContain('Home');
  });

  it('should an a tag has a .btn class', () => {
    const clsName = debugEl.query(By.css('a')).nativeElement.className;
    expect(clsName).toContain('btn');
  });

  it('should an a tag has a href /home', () => {
    const href = debugEl.query(By.css('a')).nativeElement.getAttribute('href');
    expect(href).toEqual('/home');
  });

});
