import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let debugEl: DebugElement;
  let element: HTMLElement;
  let fixture: ComponentFixture<HomeComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [HomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    element = debugEl.nativeElement;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should render Tnos in a h1 tag', () => {
    const textContent = debugEl.query(By.css('h1')).nativeElement.textContent;
    expect(textContent).toContain('Tnos');
  });

  it('should render a h2 tag', () => {
    const textContent = debugEl.query(By.css('h2')).nativeElement.textContent;
    expect(textContent).toContain('Essential Solutions For Going Abroad');
  });

  it('should render Login in a tag', () => {
    const textContent = debugEl.query(By.css('a')).nativeElement.textContent;
    expect(textContent).toContain('Login');
  });

  it('should an a tag has a .btn class', () => {
    const clsName = debugEl.query(By.css('a')).nativeElement.className;
    expect(clsName).toContain('btn');
  });

  it('should an a tag has a href /login', () => {
    const href = debugEl.query(By.css('a')).nativeElement.getAttribute('href');
    expect(href).toEqual('/login');
  });

});
