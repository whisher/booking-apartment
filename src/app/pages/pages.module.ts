import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  HomeComponent,
  ErrorComponent
} from './';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HomeComponent,
    ErrorComponent
  ],
  declarations: [
    HomeComponent,
    ErrorComponent
  ]
})
export class PagesModule { }
