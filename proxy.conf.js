const PROXY_CONFIG = [{
  context: [
    '/oauth',
    '/api',
    '/pics',
  ],
  target: 'http://212.237.12.248:8080',
  secure: false
}];

module.exports = PROXY_CONFIG;